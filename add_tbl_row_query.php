<?php
    require_once('library.php');
	
	//instantiate the various classes
	$crud = new Crud();
	$form_validate = new FormValidation();
	
	$name = $crud->mysql_prep($_POST['name']);
	$data_type = $crud->mysql_prep($_POST['data_type']);
	$length = $crud->mysql_prep($_POST['length']);
    $nullable = $crud->mysql_prep($_POST['nullable']);
    $tbl_name = $crud->mysql_prep($_POST['tbl_name']);
	
	
	//check for empty fields
	$message = $form_validate->if_empty($_POST, 
				array('name','data_type','length','nullable','tbl_name'));

    if($message != null){
        echo $message;
    }else{
        $result = $crud->alterTable($tbl_name,$name,$data_type,$nullable,$length);
        if($result){
            echo "Query Ok, New Column added";
        }else{
            echo $result;
        }
    }


?>