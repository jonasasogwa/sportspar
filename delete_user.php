<?php
	require_once('library.php');
	
	//instantiate crud function
	$crud = new Crud();
	
	//get the id sent via get request
	$id = $crud->mysql_prep($_GET['id']);
	
	$result = $crud->delete($id, 'users');
	
	if($result){
		//redirect to index if success
		header("Location: index.php");
		exit;
	}
?>