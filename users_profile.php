<?php
	require_once('library.php');
	
	
	if(isset($_GET['id'])){ $user_id = $_GET['id']; }
	
	$crud = new Crud();
	list($id,$title,$surname,$othernames,$phone_no,
					$gender,$marital_status,$password,$email,
					$user_type,$address
		) = $crud->getUsersDetails($_GET['id']);
	$fullname = $surname." ".$othernames;
?>
	
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>pickware</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link href="plugin/js/dcalendar.picker.css" rel="stylesheet" type="text/css">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="plugin/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="plugin/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="plugin/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

  <?php include_once("header.html"); ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
        <section class="content-header">
			
			<div class="callout callout-info">
			  <h4><?php echo $surname." ".$othernames."'s";?> Profile</h4>
			</div>
		
        </section>

      <!-- Main content -->
		<section class="content">
			
			<div class="row">
				<!-- /.col -->
				<div class="col-md-12 " offset='2'>
				  <div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
					  <li class="active"><a href="#activity" data-toggle="tab">View</a></li>
					  <li><a href="#update" data-toggle="tab">Update </a></li>
					</ul>
					<div class="tab-content">
					<div class="active tab-pane" id="activity">
						<!-- Post -->
						<div class="box box-primary">
					  
							<div class="timeline-item">
								<div class="box-header with-border">
								  <h3 class="box-title">PERSONAL INFORMATION</h3>
								</div>
								<?php 
									if(isset($_GET['id'])){ $user_id = $_GET['id']; }
									list($id,$title,$surname,$othernames,$phone_no,
									$gender,$marital_status,$password,$email,
									$user_type,$address) = $crud->getUsersDetails($user_id);
									$fullname = $surname." ".$othernames;
								?>
								<div class="box-body no-padding">
								  <table class="table table-condensed">
									<tr>
									  <th>TITLE</th>
									  <td><?php echo $title; ?></td>
									  <th>SURNAME</th>
									  <td ><?php echo $surname; ?></td>
									</tr>
									<tr>
									  <th>OTHER NAMES</th>
									  <td><?php echo $othernames; ?></td>
									  <th>USER TYPE</th>
									  <td><?php echo $user_type; ?></td>
									</tr>
									<tr>
									  <th>EMAIL</th>
									  <td><?php echo $email; ?></td>
									  <th>PASSWORD</th>
									  <td><?php echo $password; ?></td>
									</tr>
									
									<tr>
									  <th>GENDER</th>
									  <td><?php echo $gender ; ?></td>
									  
									</tr>
									
									<tr>
									  <th>MOBILE</th>
									  <td><?php echo $phone_no; ?></td>
									  <th>ADDRESS</th>
									  <td><?php echo $address; ?></td>
									</tr>
									
								  </table>
								  
								</div>
							  
							</div>
						</div>
						<!-- /.post -->
					</div>
					
					<div class="tab-pane" id="update">
						<section class="content">
							<div class="row">
								<div id="photo_feedback"></div>
								
								<div class="col-xs-12">
								  
									<div class="box-body">
										
										<form action="" method="post" id="" class="form-horizontal"  >
											
											<div class="col-sm-12">
												<div class="box box-primary">
													<div class="box-header">

													  <div class="box-tools pull-right">
														<ul class="pagination pagination-sm inline">
														</ul>
													  </div>
													</div>
													<input type="hidden" name="user_id" id="user_id"  value="<?php echo $_GET['id']; ?>" >
													<div class="form-group">
														<label for="title" class="col-sm-3 control-label">TITLE</label>
														<div class="col-sm-9">
															<select name="title" id="title" class="form-control select2" style="width: 100%;">
																<option value="MASTER">MASTER</option>
																 <option value="MISS">MISS</option>
																 <option value="MR">MR</option>
																 <option value="MRS">MRS</option>
																 <option value="DR">DR</option>
																 <option value="PROF">PROF</option>
															</select>
														</div>
													</div>
													
													  <div class="form-group">
														<label for="surname" class="col-sm-3 control-label">SURNAME<span style="color:red;">*</span></label>
														<div class="col-sm-9">
														  <input type="text" class="form-control" name="surname" id="surname"  value="<?php echo $surname; ?>" placeholder="Surname...">
														</div>
													  </div>
													  
													  <div class="form-group">
														<label for="othernames" class="col-sm-3 control-label">OTHER NAMES<span style="color:red;">*</span></label>
														<div class="col-sm-9">
														  <input type="text" class="form-control" name="othernames" id="othernames" value="<?php echo $othernames; ?>"  placeholder="Jonas Chinagorom...">
														</div>
													  </div>
													  
													  <div class="form-group">
														 <label for="gender" class="col-sm-3 control-label">GENDER</label>
														 <div class="col-sm-9">
															 <select name="gender" id="gender" class="form-control select2" style="width: 100%;">
																 <option value="MALE">MALE</option>
																 <option value="FEMALE">FEMALE</option>
															 </select>
														 </div>
													  </div>
													 
													<div class="form-group">
														<label class="col-sm-3  control-label" for="user_type">USER TYPE</label>
														<div class="col-sm-9">
															<select name="user_type" id="user_type" class="form-control select2" style="width: 100%;">
																<option value="ADMIN">ADMIN </option>
																<option value="VISITOR">VISITOR </option>
															</select>
														</div>
													</div>
													  
													<div class="form-group">
														<label class="col-sm-3  control-label" for="marital_status">MARITAL STATUS</label>
														<div class="col-sm-9">
															<select name="marital_status" id="marital_status" class="form-control select2" style="width: 100%;">
																<option value="">SELECT STATUS...</option>
																<option value="SINGLE">SINGLE</option>
																<option value="MARRIED">MARRIED </option>
															</select>
														</div>
													</div>
													
													<div class="form-group">
														<label for="address" class="col-sm-3 control-label">ADDRESS</label>

														<div class="col-sm-9">
															<input type="text" class="form-control" id="address" name="address" value="<?php echo $address; ?>">
														</div>
													</div>
												
													<div class="form-group">
														<label for="email" class="col-sm-3 control-label">EMAIL<span style="color:red;">*</span></label>
														<div class="col-sm-9">
														  <input type="email" class="form-control" id="email" name="email"  value="<?php echo $email; ?>" placeholder="someone@me.com">
														</div>
													</div>
												
													<div class="form-group">
														<label for="phone" class="col-sm-3 control-label">PHONE<span style="color:red;">*</span></label>
														<div class="col-sm-9">
														  <input type="text" class="form-control" id="phone" name="phone"  value="<?php echo $phone_no; ?>" placeholder="08030000000">
														</div>
													</div>
													
													<div class="form-group">
														<label for="password" class="col-sm-3 control-label">PASSWORD<span style="color:red;">*</span></label>
														<div class="col-sm-9">
														  <input type="text" class="form-control" id="password" name="password"  value="" placeholder="pass1234">
														</div>
													</div>
													
												</div>	
											</div>
											
											<div class="col-sm-12">
												<div class="box box-default">
												  <div class="box-footer clearfix no-border">
													<button type="button" id="submit" name="submit" class="btn btn-primary pull-right"><i class="fa fa-plus"></i>SUBMIT DETAILS</button>
												  </div>
												</div>
											</div>
											
										</form>
									
									</div>
								
								  <!-- /.box -->
								</div>
							</div>
						</section>
					</div>
					
					  <!-- /.tab-pane -->
					</div>
					<!-- /.tab-content -->
				  </div>
				  <!-- /.nav-tabs-custom -->
				</div>
				<!-- /.col -->
			</div>
      <!-- /.row -->
		</section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <?php include_once("footer.html"); ?>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="plugin/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="plugin/bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugin/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugin/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="plugin/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="plugin/dist/js/demo.js"></script>

	<script>
		$('#submit').click(function() {
			var user_id = $("#user_id").val();
			var title = $("#title").val();
			var surname = $("#surname").val();
			var othernames = $("#othernames").val();
			var gender = $("#gender").val();
			var user_type = $("#user_type").val();
			var marital_status = $("#marital_status").val();
			var address = $("#address").val();
			var email = $("#email").val();
			var phone = $("#phone").val();
			var password = $("#password").val();
			
			$.ajax({
				url: "update.php",
				method: "post",
				data:{
					user_id:user_id,
					title:title,
					surname:surname,
					othernames:othernames,
					gender:gender,
					user_type:user_type,
					marital_status:marital_status,
					address:address,
					email:email,
					phone:phone,
					password:password
				},
				success: function(response){
					alert(response);
				}
			});
		});
		
	</script>

</body>
</html>
