<?php
	require_once('library.php');
?>
<?php
    $crud = new Crud();
    $form_validate = new FormValidation();
   
	$tbl = $crud->mysql_prep($_POST['tbl']);
    $pk_name = $crud->mysql_prep($_POST['pk_name']);
    $length = $crud->mysql_prep($_POST['length']);

    $message = $form_validate->if_empty($_POST, 
				array('tbl','pk_name','length'));
    
                
    if($message != null){
        $msg = "<section class='alert alert-danger'>";
		  $msg .= "<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>";
		$msg .= "$message";
		$msg .= "</section>";
		echo $msg;
    }else{
        
        $result = $crud->createTable($tbl,$pk_name,$length);
        if($result){
			$output = "<div class='alert alert-success  alert-sm'>";
			  $output .= "<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>";
			  $output .= "<strong>Query Ok, New Table Created!</strong>";
			$output .= "</div>";
			echo $output;
		}
    }
	
?>