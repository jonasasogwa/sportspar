### README ###


*  Please create a database and name it mini_database 
*  Import the database mini_database.sql into the database created in 1
*  use the settings in database.php for your configuration 
*  put the entire folder on your localhost dir and run localhost/sportspar on your browser


### HOW TO USE ###
 
 * Use the menu to navigate to various parts of the application 
 * Under the menu items you will find links to add new user and also add database tables
 * use the settings col to manipulate info in the table.
 * In the db tables section, there is add table button at the top-right that can be used to add new table 