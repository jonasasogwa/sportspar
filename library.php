<?php 
require_once('database.php');
	
	class Crud extends DbConfig
	{
		//create a constructor 
		public function __construct(){
			parent::__construct();
		}
		
		//function to fetch db_data
		public function getDbData($query){
			$result = $this->connection->query($query);
			
			if($result == false){
				return false;
			}
			
			$rows = array();
			
			//loop through the result
			while($row = $result->fetch_assoc()){
				$rows[] = $row;
			}
			
			return $rows;
		}
		
		//function to execut query
		public function execute($query){
			$result = $this->connection->query($query);
			
			if($result == false){
				echo 'Error: Query failed! '.mysqli_error($this->connection);
				return false;
			}else{
				return true;
			}
		}
		
		//function to delete from database
		public function delete($id, $tbl){
			$query = "DELETE FROM $tbl WHERE id = $id";
			
			$result = $this->connection->query($query);
			
			if($result == false){
				echo 'Error: Could not Delete user with id '.$id;
				return false;
			}else{
				return true;
			}
		}
		
		function getUsersDetails($user_id){
			$query = " SELECT * FROM users WHERE id =  $user_id";
			$result = $this->connection->query($query);
			
			if($result == false){
				return false;
			}
			//loop through the result
			$row = $result->fetch_assoc();
			$id = $row['id'];
			$title = $row['title'];
			$surname = $row['surname'];
			$othernames = $row['othernames'];
			$phone_no = $row['phone_no'];
			$gender = $row['gender'];
			$marital_status = $row['marital_status'];
			$password = $row['password'];
			$email = $row['email'];
			$user_type = $row['user_type'];
			$address = $row['address'];
			
			return array($id,$title,$surname,$othernames,$phone_no,
						$gender,$marital_status,$password,$email,
						$user_type,$address);
		}
		
		function getAllTbl($tbl){
			$query = " SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE 
						TABLE_TYPE = 'BASE TABLE' AND TABLE_SCHEMA = '$tbl' ";
			$result = $this->getDbData($query);
			return $result;
		}

		function alterTable($tbl,$col,$data_type,$nul,$length){
			$query = " ALTER TABLE $tbl
						ADD COLUMN $col $data_type($length) $nul ";
			$result = $this->execute($query);
			return $result;
		}

		function createTable($tbl,$primary,$length){
			$query = "CREATE TABLE $tbl (
				$primary INT($length) UNSIGNED AUTO_INCREMENT PRIMARY KEY
				)";
			$result = $this->execute($query);
			return $result;
		}
		//house keeping
		function mysql_prep($value){
			return $this->connection->real_escape_string($value);
		}
	}
	
	class FormValidation
	{
		public function if_empty($data, $fields){
			$message = null;
			foreach($fields as $val){
				if(empty($data[$val])){
					$message .= "$val: field empty \n";
				}
			}
			return $message;
		}
		public function is_valid_email($email){
			if(filter_var($email,FILTER_VALIDATE_EMAIL)){
				return true;
			}
			return false;
		}
	}
	/*End  */
?>