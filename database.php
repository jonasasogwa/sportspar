<?php

class DbConfig
{
	//decleare connection variables
	private $_host = 'localhost';
	private $_dbuser = 'mini_users';
	private $_dbpassword = 'pass1234';
	private $_database = 'mini_database';
	
	//decleare connection string variable
	protected $connection;
	
	public function __construct()
	{
		if(!isset($this->connection)){
			$this->connection = new mysqli($this->_host, $this->_dbuser, $this->_dbpassword, $this->_database);
		    
			if(!$this->connection){
				echo 'Unable to connect to database';
				echo "Error: Unable to connect to MySQL." . PHP_EOL;
				echo "<br/>Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
				echo "<br/>Debugging error: " . mysqli_connect_error() . PHP_EOL;
				exit;
			}
		
		}
		return $this->connection;
	}
}

?>