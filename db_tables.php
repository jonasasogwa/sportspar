<?php 
	require_once('library.php');
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>pickware</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="plugin/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="plugin/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="plugin/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

  <?php include_once("header.html"); ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
		<section class="content">
			
			<div class="active tab-pane" id="activity">
				<!-- Post -->
				<div class="row">
                    
					<div class="col-xs-12">
                    
					  <div class="box">
                          
						<div class="box-header">
							<div class="callout callout-info lead">
								<h4  id="msg">LIST OF DB TABLES</h4>
							</div>
                        </div>
                        
						<?php
							//instanciate the crud class
							$crud = new Crud();
							//query the users database
                            $result = $crud->getAllTbl('mini_database');
						?>
						<!-- /.box-header -->
						<div class="table-responsive box-body">

                            <span class="pull-right">
                                <div id="marksheet" class="btn btn-primary fa fa-plus" data-toggle="modal" 
                                data-target="#addCalender" >Add Table</div>
                            </span><br/>

							<link href="css1/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
							<link href="css1/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
							<table id="example1" class="table table-responsive table-bordered table-hover  ">
								<thead>
									<tr>
									   <th>S/N</th>
                                       <th>NAME</th>
                                       <th>ROWS</th>
                                       <th>CREATE TIME</th>
									   <th>SETTINGS</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$sn = 1;
										foreach($result as $key=>$val){
									?>
									<tr>
										<td><?php echo  $sn; ?> </td>
                                        <td><?php echo  $val['TABLE_NAME']; ?> </td>
                                        <td><?php echo  $val['TABLE_ROWS']; ?> </td>
                                        <td><?php echo $val['CREATE_TIME']; ?></td>
										<td>
											<div class="btn-group">
                                              <a class="btn btn-success btn-xs"   
                                                href="add_tbl_rows.php?id=<?php echo $val['TABLE_NAME'];?>"  > 
                                              <i class="fa fa-eye">Add Col</i> </a>
                                              
											</div>
										</td>
									</tr>
									<?php
										$sn++;
										} 
									?>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
					  </div>
					  <!-- /.box -->
					</div>
				</div>
				<!-- /.post -->
			</div>
			
        </section>
        
        
        <div class="modal fade" id="addCalender" role="dialog">
            <div class="modal-dialog modal-primary " >
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">ADD TABLE </h4>
                </div>
                
                <div class="modal-body"  id="addContent">
                <div id="pro_msg"></div>
                <form method="POST" action="db_tbl_query.php" id="msg_from1" >

                    <input type="hidden" class="form-control" id="id" name="id" value="" />
                    
                    <div class="row">
                        <div class="col-sm-12" style="">
                            <label class=" control-label" for="tbl" style="font-size:11px;">TABLE NAME<span style="color:red;">*</span></label>
                            <div class="form-group">
                                <input type="text" class="form-control" id="tbl" name="tbl" value="" />
                            </div>
                        </div>	
                    </div>

                    <fieldset>                    
                        <legend class=" control-label" style="font-size:11px; color:white;">Primary Key</legend>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label for="pk_name" class="control-label" style="font-size:11px;">NAME</label>
                                <input name="pk_name" class=" form-control " id="pk_name" type="text">
                            </div>
                            
                            <div class="col-sm-6 form-group">
                                <label for="length" class="control-label" style="font-size:11px;">LENGTH</label>
                                <input name="length" class=" form-control " id="length" type="number">
                            </div>

                        </div>
                    </fieldset>
                
                </div>
                
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" id="submit" class="btn btn-primary pull-right" >Submit</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
            </div>
        </div>


      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <?php include_once("footer.html"); ?>
</div>	
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="plugin/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="plugin/bootstrap/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="plugin/dist/js/app.min.js"></script>
	<script>

        $(document).ready(function(){
			$("#submit").click(function(){
				var data = $("#msg_from1 :input").serializeArray();
				$.post($("#msg_from1").attr("action"), data, function(info){
					$("#pro_msg").html(info);
                   
				});
				$("#msg_from1").submit(function(){
					return false;
				});
                
			});
		});

        function reload() {
            document.location.reload();
        }

		$(document).ready(function() {
			$('#example1').DataTable( {
				dom: 'Bfrtip',
				buttons: [
					'copy', 'csv', 'excel', 'pdf', 'print'
				]
			} );
		});
	</script>

<!-- EXPORTING PAYMENT -->
	<script type="text/javascript" src="js1/jquery.dataTables.min.js"> </script>
	<script type="text/javascript" src="js1/dataTables.buttons.min.js"> </script>
	<script type="text/javascript" src="js1/buttons.flash.min.js" ></script>
	<script type="text/javascript" src="js1/jszip.min.js"> </script>
	<script type="text/javascript" src="js1/pdfmake.min.js"> </script>
	<script type="text/javascript" src="js1/vfs_fonts.js"> </script>
	<script type="text/javascript" src="js1/buttons.html5.min.js"> </script>
	<script type="text/javascript" src="js1/buttons.print.min.js"> </script>

</body>
</html>
