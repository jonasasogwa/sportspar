<?php 
	require_once('library.php');
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>pickware</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="plugin/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="plugin/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="plugin/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

  <?php include_once("header.html"); ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
		<section class="content">
			
			<div class="active tab-pane" id="activity">
				<!-- Post -->
				<div class="row">
					<div class="col-xs-12">
					  <div class="box">
						<div class="box-header">
							<div class="callout callout-info lead">
								<h4  id="msg">LIST OF USERS</h4>
							</div>
						</div>
						
						<?php
							//instanciate the crud class
							$crud = new Crud();
							
							//query the users database
							$query = " SELECT * FROM users ORDER BY surname ASC";
							$result = $crud->getDbData($query);
						?>
						
						<!-- /.box-header -->
						<div class="table-responsive box-body">
							<link href="css1/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
							<link href="css1/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
							<table id="example1" class="table table-responsive table-bordered table-hover  ">
								<thead>
									<tr>
									   <th>S/N</th>
									   <th>TITLE</th>
									   <th>SURNAME</th>
									   <th>OTHER_NAMES</th>
									   <th>GENDER</th>
									   <th>MARITAL STATUS</th>
									   <th>PHONE</th>
									   <th>EMAIL</th>
									   <th>SETTINGS</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$sn = 1;
										foreach($result as $key=>$val){
									?>
									<tr>
										<td><?php echo  $sn; ?> </td>
										<td><?php echo  $val['title']; ?> </td>
										<td><?php echo  $val['surname']; ?> </td>
										<td><?php echo  $val['othernames']; ?> </td>
										<td><?php echo  $val['gender']; ?> </td>
										<td><?php echo  $val['marital_status']; ?> </td>
										<td><?php echo  $val['phone_no']; ?> </td>
										<td><?php echo  $val['email']; ?> </td>
										<td>
											<div class="btn-group">
											  <a class="btn btn-success btn-xs"   href="users_profile.php?id=<?php echo $val['id'];?>"  > 
											  <i class="fa fa-eye">view/edit</i> </a>
											  <a class="btn btn-danger btn-xs" onclick="return confirm('Are you sure you want to delete?');"
													href="delete_user.php?id=<?php echo $val['id']; ?>" > <i class="fa fa-trash">trash</i></a>
											</div>
										</td>
									</tr>
									<?php
										$sn++;
										} 
									?>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
					  </div>
					  <!-- /.box -->
					</div>
				</div>
				<!-- /.post -->
			</div>
			
		</section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <?php include_once("footer.html"); ?>
</div>	
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="plugin/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="plugin/bootstrap/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="plugin/dist/js/app.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#example1').DataTable( {
				dom: 'Bfrtip',
				buttons: [
					'copy', 'csv', 'excel', 'pdf', 'print'
				]
			} );
		});
	</script>

<!-- EXPORTING PAYMENT -->
	<script type="text/javascript" src="js1/jquery.dataTables.min.js"> </script>
	<script type="text/javascript" src="js1/dataTables.buttons.min.js"> </script>
	<script type="text/javascript" src="js1/buttons.flash.min.js" ></script>
	<script type="text/javascript" src="js1/jszip.min.js"> </script>
	<script type="text/javascript" src="js1/pdfmake.min.js"> </script>
	<script type="text/javascript" src="js1/vfs_fonts.js"> </script>
	<script type="text/javascript" src="js1/buttons.html5.min.js"> </script>
	<script type="text/javascript" src="js1/buttons.print.min.js"> </script>

</body>
</html>
