<?php
	require_once('library.php');
	
	//instantiate the various classes
	$crud = new Crud();
	$form_validate = new FormValidation();
	
	$title = $crud->mysql_prep($_POST['title']);
	$surname = $crud->mysql_prep($_POST['surname']);
	$othernames = $crud->mysql_prep($_POST['othernames']);
	$gender = $crud->mysql_prep($_POST['gender']);
	$user_type = $crud->mysql_prep($_POST['user_type']);
	
	$marital_status = $crud->mysql_prep($_POST['marital_status']);
	$address = $crud->mysql_prep($_POST['address']);
	$email = $crud->mysql_prep($_POST['email']);
	$phone = $crud->mysql_prep($_POST['phone']);
	$password = $crud->mysql_prep($_POST['password']);
	//$password = MD5($password);
	
	//check for empty fields
	$message = $form_validate->if_empty($_POST, 
				array('title','surname','othernames','gender','user_type'
				,'marital_status','address','email','phone','password'));

	$check_email = $form_validate->is_valid_email($email);
	
	if($message != null){
		echo $message;
	}elseif(!$check_email){
		echo 'Please enter a valid email';
	}else{
		$result = $crud->execute("INSERT INTO users(title, surname, othernames, gender, phone_no, 
								marital_status, password, email, user_type, address) 
							VALUES('$title','$surname','$othernames','$gender','$phone','$marital_status','$password',
							'$email','$user_type','$address') ");
		if($result){
			
			echo "Query Ok, New user Added";
		}
	}
	
?>