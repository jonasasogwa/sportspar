-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 27, 2020 at 04:14 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mini_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `title` enum('MASTER','MISS','MR','MRS','DR','PROF') NOT NULL DEFAULT 'MISS',
  `surname` varchar(20) NOT NULL,
  `othernames` varchar(30) NOT NULL,
  `phone_no` varchar(15) NOT NULL,
  `gender` enum('MALE','FEMALE') NOT NULL DEFAULT 'MALE',
  `marital_status` enum('SINGLE','MARRIED','DIVORCED','SEPARATED','CELIBATE') NOT NULL DEFAULT 'SINGLE',
  `password` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `user_type` enum('ADMIN','ACADEMIC') NOT NULL DEFAULT 'ACADEMIC',
  `address` varchar(60) NOT NULL,
  `location` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `title`, `surname`, `othernames`, `phone_no`, `gender`, `marital_status`, `password`, `email`, `user_type`, `address`, `location`) VALUES
(1, 'MISS', 'Asogwa', 'Jonas C', '07061548268', 'MALE', 'SINGLE', 'passm3', 'admin@school.com', 'ADMIN', '11 Ezimo Street Orba Road, Nsukka', ''),
(4, 'MASTER', 'Matins', 'Ezenwa', '09082764827', 'MALE', 'SINGLE', 'passm2', 'matins@gmail.com', 'ADMIN', '44 modupe young thomas estate, ajah', ''),
(5, 'MASTER', 'Jay', 'Asogwa', '0908276045', 'MALE', 'MARRIED', 'pass1234', 'jay@gmail.com', 'ADMIN', '11 Ezimo Street Orba Road, Nsukka', ''),
(7, 'MASTER', 'Jackson', 'Family', '015124350384', 'MALE', 'MARRIED', 'passup', 'chinagorom@gmail.com', 'ADMIN', 'Max-Planck Str., 2', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
