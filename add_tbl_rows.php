<?php
	require_once('library.php');
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>pickware</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="plugin/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="plugin/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="plugin/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

<?php include_once("header.html"); ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
		<section class="content">
		
		<div class="row">
			<div class="col-md-12">
			  <div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
				  <li class="active"><a href="#sort_by_class" data-toggle="tab">Add Col</a></li>
				</ul>
				
				<div class="tab-content">
				  <!-- /.tab-pane -->
					<div class="active tab-pane" id="sort_by_class">
						<section class="content">
							<div class="row">
								<div class="col-xs-12">
								<div class="box-header">
									<div class="callout callout-info lead">
										<h4  id="msg">ADD COL</h4>
									</div>
								</div>
								<div class="table-responsive box-body">
									<table id="example1" class="table table-responsive table-bordered table-hover  ">
										<thead>
											<tr>
												<th>NAME</th>
												<th>TYPE</th>
												<th>LENGHT</th>
												<th>NULL?</th>
											</tr>
										</thead>
										<tbody>
										<form action="" method="post" id="add_doc_form" class="form-horizontal"  >
											
											<tr>
												<td>
												<input type="text" class="form-control" id="name" name="name" value="">
												</td>
												<td>
												<select name="data_type" id="data_type" class="form-control select2" style="width: 100%;">
													<option value="VARCHAR">VARCHAR </option>
													<option value="INT">INT </option>
												</select>
												</td>
												<td>
												<input type="number" class="form-control" id="length" name="length" value="">
												</td>
												<td>
												
												<select name="nullable" id="nullable" class="form-control select2" style="width: 100%;">
													<option value="NULL">NULL </option>
													<option value="NOT NULL">NOT NULL </option>
												</select>
												</td>
											</tr>

											<tr>
												<td colspan="4">
													<button type="button" id="submit" name="submit" class="btn btn-primary pull-right">
														<i class="fa fa-plus"></i>SAVE</button>
  												</td>
											</tr>
											<input type="hidden" class="form-control" id="tbl_name" name="tbl_name" value="<?php echo $_GET['id']; ?>">
  										</form>
										</tbody>
									</table>
									
								</div>

								</div>
							</div>
						</section>
					</div>  
					
				</div>
				<!-- /.tab-content -->
			  </div>
			  <!-- /.nav-tabs-custom -->
			</div>
		</div>
			
		</section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <?php include_once("footer.html"); ?>
</div>	
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="plugin/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="plugin/bootstrap/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="plugin/dist/js/app.min.js"></script>
	<script>
		$('#submit').click(function() {
			var name = $("#name").val();
			var data_type = $("#data_type").val();
			var length = $("#length").val();
			var nullable = $("#nullable").val();
			var tbl_name = $("#tbl_name").val();
			
			$.ajax({
				url: "add_tbl_row_query.php",
				method: "post",
				data:{
					name:name,
					data_type:data_type,
					length:length,
					nullable:nullable,
					tbl_name:tbl_name
				},
				success: function(response){
					alert(response);
					location.reload(true)
				}
			});
		});
		
	</script>
	

</body>
</html>
